#!/usr/bin/perl
#Converts XMFA files to FASTA
#Author: Maude Jacquot 
#Date: 2016-11
#Usage:
#Output to STDOUT:     xmfa2fasta.pl [--align] --file <XMFA file>
#Output to FASTA file: xmfa2fasta.pl [--align] --file <XMFA file> > <FASTA file>
use strict;
use warnings;
use 5.010;
use Term::Cap;
use POSIX;
use Getopt::Long qw(:config no_ignore_case);

my %opts;
GetOptions(
	'h|help'        => \$opts{'h'},
	'f|file=s'      => \$opts{'f'}
) or die "Error in command line arguments\n";
my $infile = $opts{'f'};

if ( $opts{'h'} ) {
	show_help();
	exit;
}

if ( !$infile ) {
	say "Usage: xmfa2fasta.pl --file <XMFA filename>";
	say "See xmfa2fasta.pl --help for more options.";
	exit 1;
}
main();
exit;



sub main {
	
	my $current_seq_nb=0;
	my $current_seq="";	
	my @current_LCB=();
	my @LCBs;
	my $nb_seq=0;
	my @seq_names;
	my $nb_LCB=1;

	open( my $fh, '<', $infile ) or die "Cannot open file $infile\n";
	# open xmfa file
	while ( my $line = <$fh> ) {
		
		if ( $line =~ /^\#Sequence.*File/ ) {
			$nb_seq++;
			my @name = split(/\s/,$line);
			$name[1]=~ s/.fasta//;
			$name[1]=~ s/snp_indel//;
			push(@seq_names,$name[1]);			
		}
		
		if ( $line =~ /^=/ ) {
			
			# save ended LCB alignment
			my $current_LCB_size= @current_LCB ;
			if ( $current_LCB_size eq $nb_seq-1 ) {
				push( @current_LCB, $current_seq);
				
				my $LCBs_size= @LCBs ;
				if ( $LCBs_size>0 ) {
					for (my $i = 0; $i <= $nb_seq-1; $i++) {
   						$LCBs[$i]=$LCBs[$i].$current_LCB[$i];
   					}
				}
				else {@LCBs=@current_LCB;}
			}
									
			# Create a new LCB and re-inatialize variables 
			$nb_LCB++;
			@current_LCB=();
			$current_seq_nb=0;	
			$current_seq="";	
		}
		
		if ( $line =~ /^>\s*(.+):/x ) {
			if ( $current_seq_nb>0 ) {
				push( @current_LCB, $current_seq);
			}
			$current_seq_nb++;	
			$current_seq="";	

		}
		
		
		if ( $line =~ /^[A,T,C,G,N,-]/) {
			$current_seq=$current_seq.$line;
		}

		
		
	}
	#close xmfa
	close $fh;
	
	for (my $i = 0; $i <= $nb_seq-1; $i++) {
		print ">$seq_names[$i]\n";
		print "$LCBs[$i]\n";
	}	

	return;
}



sub show_help {
	my $termios = POSIX::Termios->new;
	$termios->getattr;
	my $ospeed = $termios->getospeed;
	my $t = Tgetent Term::Cap { TERM => undef, OSPEED => $ospeed };
	my ( $norm, $bold, $under ) = map { $t->Tputs( $_, 1 ) } qw/me md us/;
	say << "HELP";
${bold}NAME$norm
    ${bold}xmfa2fasta.pl$norm - Convert XMFA file to FASTA file
    
${bold}SYNOPSIS$norm
    ${bold}xmfa2fasta.pl --file ${under}XMFA_FILE$norm [${under}options$norm]
    
    Output is directed to STDOUT.  Usually you will want to create a new
    FASTA file so direct output to the new file, e.g.
    
    ${bold}xmfa2fasta.pl --file ${under}XMFA_FILE$norm [${under}options$norm] >  ${bold}${under}FASTA_FILE$norm
${bold}OPTIONS$norm


${bold}-f, --file$norm ${under}FILE$norm  
    XMFA file.
    
${bold}-h, --help$norm
    This help page.    

HELP
	return;
}