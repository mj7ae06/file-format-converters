######################################################################################
# This script takes alignment sequence phylip file and converts it to fasta file 
# Author: Maude Jacquot
# Date: 2016-09-16
# Usage: perl phylip2fasta.pl inputPhilipFile
######################################################################################

use strict;
use warnings;
use Bio::AlignIO; 

# http://doc.bioperl.org/bioperl-live/Bio/AlignIO.html
# http://doc.bioperl.org/bioperl-live/Bio/AlignIO/phylip.html
# http://www.bioperl.org/wiki/PHYLIP_multiple_alignment_format

my ($inputfilename) = @ARGV;
die "must provide phylip file as 1st parameter...\n" unless $inputfilename;
my $in  = Bio::AlignIO->new(-file   => $inputfilename ,
                         -format => 'phylip',
                         -interleaved => 1);
my $out = Bio::AlignIO->new(-fh   => \*STDOUT ,
                         -format => 'fasta');

while ( my $aln = $in->next_aln() ) {
    $out->write_aln($aln);
}