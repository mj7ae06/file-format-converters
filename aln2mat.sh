#!/bin/bash

######################################################################################
# This script converts sequence alignment in fasta format to numerical matrix in MAPIT format
# Author: Maude Jacquot
# Date: 2016-01-29
# Usage: ./aln2mat.sh inputFastaFile outputMatFile
######################################################################################

seq="";

while read line
do

  if [ ${line:0:1} == ">" ]; then
    if [ "$seq" != "" ]; then 
    	echo $seq | sed $'s/A/1\t/g' | sed $'s/T/2\t/g' | sed $'s/C/3\t/g' | sed $'s/G/4\t/g' | sed $'s/[KMRYSWBVHD]/5\t/g'; # num seq

    fi
    seq="";
    #echo $line;
  else
  seq=$seq$line;
  fi
done <$1

echo $seq | sed $'s/A/1\t/g' | sed $'s/T/2\t/g' | sed $'s/C/3\t/g' | sed $'s/G/4\t/g' | sed $'s/[KMRYSWBVHD]/5\t/g'; # num seq
